import React  from 'react'
import Slider from './Slider';
import './App.css';
export default class App extends React.Component{
     
    render(){         
        return(
            <div className="App" style={{backgroundColor:"#e6f8f6",height:"41.6rem"}}>  
                <h1 style={{color:"#03414d",textAlign:"center",paddingTop:"50px"}} >Lifecycle Components</h1>
                <Slider />
            </div>
        )
    }
}


